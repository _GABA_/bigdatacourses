import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.yasiuraroman.CompositeKey;
import com.yasiuraroman.MyMapper;
import com.yasiuraroman.MyReducer;
import junit.framework.TestCase;
import org.apache.avro.Schema;
import org.apache.avro.Schema.Parser;
import org.apache.avro.generic.GenericData.Record;
import org.apache.avro.hadoop.io.AvroSerialization;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class MapReduceTests {

    public static final String SCHEMA = "{\"type\":\"record\","
            + "\"name\":\"topLevelRecord\","
            + "\"fields\":["
            + "{\"name\":\"id\",\"type\":[\"long\",\"null\"]},"
            + "{\"name\":\"date_time\",\"type\":[\"string\",\"null\"]},"
            + "{\"name\":\"site_name\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"posa_continent\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"user_location_country\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"user_location_region\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"user_location_city\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"orig_destination_distance\",\"type\":[\"double\",\"null\"]},"
            + "{\"name\":\"user_id\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"is_mobile\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"is_package\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"channel\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"srch_ci\",\"type\":[\"string\",\"null\"]},"
            + "{\"name\":\"srch_co\",\"type\":[\"string\",\"null\"]},"
            + "{\"name\":\"srch_adults_cnt\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"srch_children_cnt\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"srch_rm_cnt\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"srch_destination_id\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"srch_destination_type_id\",\"type\":[\"int\",\"null\"]},"
            + "{\"name\":\"hotel_id\",\"type\":[\"long\",\"null\"]}]}";

public static final String KEY_SCHEMA = "{\"type\":\"record\","
            + "\"name\":\"key\","
            + "\"fields\":["
            + "{\"name\":\"hotel_id\",\"type\":[\"long\",\"null\"]},"
            + "{\"name\":\"srch_ci\",\"type\":[\"string\",\"null\"]},"
            + "{\"name\":\"id\",\"type\":[\"long\",\"null\"]}]}";

private Schema schema;
private Schema keySchema;
private AvroKey<CompositeKey> outputKey;
private AvroKey<Record> inputKey;
private AvroValue<Record> value;
private MapDriver<AvroKey<Record>, NullWritable,
        AvroKey<CompositeKey>, AvroValue<Record>> mapDriver;
private ReduceDriver<AvroKey<CompositeKey>, AvroValue<Record>,
        AvroKey<CompositeKey>, AvroValue<Record>> reduceDriver;

    @Before
    public void before() throws IOException {
    schema = new Parser().parse(SCHEMA);
    keySchema = new Parser().parse(KEY_SCHEMA);
    mapDriver = new MapDriver<>();
    reduceDriver = new ReduceDriver<>();
    Configuration conf = mapDriver.getConfiguration();
    conf.set("io.serializations","org.apache.avro.hadoop.io.AvroSerialization," +
            "org.apache.hadoop.io.serializer.WritableSerialization," +
            "org.apache.hadoop.io.serializer.JavaSerialization"
    );

    AvroSerialization.setKeyWriterSchema(conf, keySchema);
    AvroSerialization.setKeyReaderSchema(conf, keySchema);
    AvroSerialization.setValueWriterSchema(conf, schema);
    AvroSerialization.setValueReaderSchema(conf, schema);

    Record record = new Record(schema);
    record.put("srch_adults_cnt", 1);
    record.put("hotel_id", Long.valueOf(36));
    record.put("srch_ci", "2020-10-10");
    record.put("id", Long.valueOf(4));
    CompositeKey compositeKey = new CompositeKey(
            Long.parseLong(record.get("hotel_id").toString()),
            record.get("srch_adults_cnt").toString(),
            Long.parseLong(record.get("id").toString())
    );
    inputKey = new AvroKey<>(record);
    outputKey = new AvroKey<>(compositeKey);
    value = new AvroValue<>();
    value.datum(record);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver
            .withMapper(new MyMapper())
            .withInput(inputKey, NullWritable.get())
            .withOutput(outputKey, value)
            .runTest();
    }

    @Test
    public void testReducer() throws IOException {
        List<AvroValue<Record>> values = new ArrayList<>();
        values.add(value);
        reduceDriver
            .withReducer(new MyReducer())
            .withInput(outputKey, values)
            .withOutput(outputKey, value)
            .runTest();
    }

}
