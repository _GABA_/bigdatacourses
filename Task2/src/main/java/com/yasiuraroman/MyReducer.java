package com.yasiuraroman;

import org.apache.avro.generic.GenericData;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.Iterator;

public class MyReducer extends Reducer<AvroKey<CompositeKey>, AvroValue<GenericData.Record>,
        AvroKey<CompositeKey>, AvroValue<GenericData.Record>>{

    @Override
    protected void reduce(AvroKey<CompositeKey> key, Iterable<AvroValue<GenericData.Record>> values, Context context)
            throws IOException, InterruptedException {
        Iterator<AvroValue<GenericData.Record>> iterator = values.iterator();
        while(iterator.hasNext()) {
            context.write(key, iterator.next());
        }
    }
}
