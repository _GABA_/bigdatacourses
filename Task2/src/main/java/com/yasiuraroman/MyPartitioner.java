package com.yasiuraroman;

import org.apache.avro.generic.GenericData.Record;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.hadoop.mapreduce.Partitioner;

public class MyPartitioner extends Partitioner<AvroKey<CompositeKey>, AvroValue<Record>> {

    @Override
    public int getPartition(AvroKey<CompositeKey> key,
                            AvroValue<Record> value, int i) {
        return (int) key.datum().getHotel_id() % 100000;
    }

}
