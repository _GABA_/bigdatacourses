package com.yasiuraroman;

import org.apache.avro.generic.GenericData;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class MyMapper extends Mapper<AvroKey<GenericData.Record>, NullWritable,
        AvroKey<CompositeKey>, AvroValue<GenericData.Record>> {

    @Override
    protected void map(AvroKey<GenericData.Record> key, NullWritable value, Context context)
            throws IOException, InterruptedException {
        int adultsCount = (Integer) key.datum().get("srch_adults_cnt");
        if (adultsCount > 2) {
            CompositeKey compositeKey = new CompositeKey();
            compositeKey.setBooking_id(Long.parseLong( key.datum().get("id").toString()));
            compositeKey.setSrch_ci(key.datum().get("srch_ci").toString());
            compositeKey.setHotel_id(Long.parseLong( key.datum().get("hotel_id").toString()));
            context.write(new AvroKey<>(compositeKey), new AvroValue<>(key.datum()));
        }
    }
}
