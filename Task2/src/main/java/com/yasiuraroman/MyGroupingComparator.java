package com.yasiuraroman;

import org.apache.avro.generic.GenericData;
import org.apache.avro.mapred.AvroKey;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class MyGroupingComparator extends WritableComparator {

    public MyGroupingComparator() {
        super(CompositeKey.class,true);
    }

    @Override
    public int compare(WritableComparable firstWC, WritableComparable secondWC) {
        AvroKey<GenericData.Record> ck1 = (AvroKey<GenericData.Record>) firstWC;
        AvroKey<GenericData.Record> ck2 = (AvroKey<GenericData.Record>) firstWC;
        GenericData.Record firstRecord = ck1.datum();
        GenericData.Record secondRecord = ck2.datum();
        long firstHotelId = (Long) firstRecord.get("hotel_id");
        long secondHotelId = (Long) secondRecord.get("hotel_id");
        int hotelIdCmp = (int) (secondHotelId - firstHotelId);
        if (secondHotelId - firstHotelId != 0) {
            return hotelIdCmp;
        }
        String fistSrchCi = (String) firstRecord.get("srch_ci");
        String seconddSrchCi = (String) secondRecord.get("srch_ci");
        if (fistSrchCi.compareTo(seconddSrchCi) != 0) {
            return fistSrchCi.compareTo(seconddSrchCi);
        }
        long firstId = (Long) firstRecord.get("id");
        long secondId = (Long) secondRecord.get("id");
        return (int) (firstId - secondId);

    }
}
