package com.yasiuraroman;

import org.apache.avro.Schema;
import org.apache.avro.mapred.AvroValue;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.avro.mapreduce.AvroKeyInputFormat;
import org.apache.avro.mapreduce.AvroKeyValueOutputFormat;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class JobConfig {
    public static final String SCHEMA =
            "{\"type\":\"record\","
                    + "\"name\":\"topLevelRecord\","
                    + "\"fields\":["
                    + "{\"name\":\"id\",\"type\":[\"long\",\"null\"]},"
                    + "{\"name\":\"date_time\",\"type\":[\"string\",\"null\"]},"
                    + "{\"name\":\"site_name\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"posa_continent\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"user_location_country\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"user_location_region\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"user_location_city\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"orig_destination_distance\",\"type\":[\"double\",\"null\"]},"
                    + "{\"name\":\"user_id\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"is_mobile\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"is_package\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"channel\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"srch_ci\",\"type\":[\"string\",\"null\"]},"
                    + "{\"name\":\"srch_co\",\"type\":[\"string\",\"null\"]},"
                    + "{\"name\":\"srch_adults_cnt\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"srch_children_cnt\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"srch_rm_cnt\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"srch_destination_id\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"srch_destination_type_id\",\"type\":[\"int\",\"null\"]},"
                    + "{\"name\":\"hotel_id\",\"type\":[\"long\",\"null\"]}]}";

    public static final String KEY_SCHEMA =
            "{\"type\":\"record\","
                    + "\"name\":\"key\","
                    + "\"fields\":["
                    + "{\"name\":\"hotel_id\",\"type\":[\"long\",\"null\"]},"
                    + "{\"name\":\"srch_ci\",\"type\":[\"string\",\"null\"]},"
                    + "{\"name\":\"id\",\"type\":[\"long\",\"null\"]}]}";

    public void main(String[] args) throws Exception {

        Schema schema = new Schema.Parser().parse(SCHEMA);
        Schema keySchema = new Schema.Parser().parse(KEY_SCHEMA);

        Configuration config = new Configuration();
        Job job = Job.getInstance(config, "My Secondary Sort Job");
        job.setJarByClass(JobConfig.class);

        job.setMapperClass(MyMapper.class);
        job.setReducerClass(MyReducer.class);
        job.setSortComparatorClass(MyGroupingComparator.class);
        job.setInputFormatClass(AvroKeyInputFormat.class);
        job.setOutputFormatClass(AvroKeyValueOutputFormat.class);

        job.setMapOutputKeyClass(CompositeKey.class);
        job.setMapOutputValueClass(AvroValue.class);
        job.setOutputKeyClass(CompositeKey.class);
        job.setOutputValueClass(AvroValue.class);
        job.setNumReduceTasks(2);

        AvroJob.setInputKeySchema(job, schema);
        AvroJob.setMapOutputKeySchema(job, keySchema);
        AvroJob.setMapOutputValueSchema(job, schema);
        AvroJob.setOutputKeySchema(job, keySchema);
        AvroJob.setOutputValueSchema(job, schema);

        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }

}
