package com.yasiuraroman;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

public class CompositeKey implements Writable, WritableComparable<CompositeKey> {

    private long hotel_id;          // PK
    private String srch_ci;         // FK
    private long booking_id;        // FK

    public CompositeKey() {
    }

    public CompositeKey(Long hotel_id, String srch_ci, Long booking_id) {
        super();
        this.hotel_id = hotel_id;
        this.srch_ci = srch_ci;
        this.booking_id = booking_id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(hotel_id, srch_ci, booking_id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CompositeKey other = (CompositeKey) o;
        return hotel_id == other.hotel_id &&
                booking_id == other.booking_id &&
                Objects.equals(srch_ci, other.srch_ci);
    }

    public int compareTo(CompositeKey o) {
        int comparingValue = Long.compare(this.hotel_id, o.hotel_id);
        if (comparingValue != 0) {
            return comparingValue;
        }
        comparingValue = this.srch_ci.compareTo(o.srch_ci);
        if (comparingValue != 0) {
            return comparingValue;
        }
        return Long.compare(this.booking_id, o.booking_id);
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeLong(hotel_id);
        out.writeUTF(srch_ci);
        out.writeLong(booking_id);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.setHotel_id(in.readLong());
        this.setSrch_ci(in.readUTF());
        this.setBooking_id(in.readLong());
    }

    public long getHotel_id() {
        return hotel_id;
    }

    public void setHotel_id(Long hotel_id) {
        this.hotel_id = hotel_id;
    }

    public String getSrch_ci() {
        return srch_ci;
    }

    public void setSrch_ci(String srch_ci) {
        this.srch_ci = (srch_ci == null) ? "" : srch_ci;
    }

    public long getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(Long booking_id) {
        this.booking_id = booking_id;
    }
}
