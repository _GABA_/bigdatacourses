import java.sql.Timestamp

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.elasticsearch.hadoop.cfg.ConfigurationOptions

import scala.util.matching.Regex


object Consumer {
    
    val CENSOR_SUBSTITUTE = "***"
    val messagePattern: Regex = """(?s)\{.*"speaker": *"(\w+)", *.*"time": *"(.+)", *.*"word": *"(.+)".*\}""".r
    
    def main(args: Array[String]): Unit = {
        
        
        if (args.length !=4 ){
            throw new IllegalArgumentException;
        }
        
        val spark = SparkSession
            .builder()
            .config(ConfigurationOptions.ES_NET_HTTP_AUTH_USER, "elastic")
            .config(ConfigurationOptions.ES_NET_HTTP_AUTH_PASS, "changeme")
            .config(ConfigurationOptions.ES_NODES, "127.0.0.1")
            .config(ConfigurationOptions.ES_PORT, "9200")
            .config(ConfigurationOptions.ES_INDEX_AUTO_CREATE, "true")
            .config(ConfigurationOptions.ES_NODES_WAN_ONLY, "true")
            .appName("Streaming")
            .master("local")
            .getOrCreate()
        
        spark.sparkContext.setLogLevel("ERROR")
        
        val HOSTS_PORTS = args(0)
        val TOPIC = args(1)
        val DICTIONARY: Array[String] = spark.sparkContext.textFile(args(2))
            .flatMap(line => line.split("\n")).collect()
        val WINDOW_DURATION = args(3).toInt
        
        import spark.implicits._
        val inputStream = spark.readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", HOSTS_PORTS)
            .option("subscribe", TOPIC)
            .option("failOnDataLoss","false")
            .load()
        
        val value = inputStream.selectExpr("CAST(value AS STRING)")
            .as[String]
        
        
        
        val message = value
            .flatMap( v => parseMessage(v))
            .map( m => censorMessage(m,DICTIONARY))
            .as[Message]
        
        val speakerWordFromWindow = message
            .selectExpr("*","cast(time as timestamp) as e_time")
            .withWatermark("e_time", "10 minutes")
            .writeStream
            .outputMode("append")
            .format("es")
            .option("checkpointLocation", "D:\\logs")
            .option("truncate", false)
            .start("my_data_01")
        
        speakerWordFromWindow.awaitTermination()
        
        spark.stop()
    }
    
    def parseMessage(text: String):Iterator[Message] = {
        messagePattern.findAllIn(text)
            .matchData
            .map( m =>  Message( m.group(1), m.group(2) , m.group(3)) )
    }
    
    def censorMessage(message: Message,dictionary:Array[String]):Message = {
        if (dictionary.contains(message.word)){
            return Message(message.speaker,message.time,CENSOR_SUBSTITUTE)
        }
        message
    }
    
}

case class Message(speaker: String, time: String, word: String){
    override def toString: String = s"speaker: ${speaker}, time: ${time}, word: ${word}"
    
}
