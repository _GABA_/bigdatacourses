package producer

import java.util.Properties

import org.apache.hadoop.conf.Configuration
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.spark.sql.SparkSession

object Producer {
    
    /**
     * arg(0)  массив хост:порт коодинат до кафка кластеру
     * arg(1)  назва топіка
     * arg(2)  масив імен спікерів
     * arg(3)  шлях до словника
     */
    def main(args: Array[String]): Unit = {
    
        if (args.length !=4 ){
            throw new IllegalArgumentException;
        }
        
        val spark = SparkSession
            .builder
            .config("spark.master", "local")
            .config("spark.hadoop.validateOutputSpecs", "false")
            .getOrCreate()
    
        val hadoopConfig: Configuration = spark.sparkContext.hadoopConfiguration
    
        hadoopConfig.set("fs.hdfs.impl", classOf[org.apache.hadoop.hdfs.DistributedFileSystem].getName)
    
        hadoopConfig.set("fs.file.impl", classOf[org.apache.hadoop.fs.LocalFileSystem].getName)
        
        spark.sparkContext.setLogLevel("ERROR")
        
        val HOSTS_PORTS = args(0)
        val TOPIC = args(1)
        val SPEAKERS = args(2).split(",")
        val DICTIONARY: Array[String] = spark.sparkContext.textFile(args(3))
            .flatMap(line => line.split("\n")).collect()
    
        val messageGenerator = new MessageGenerator(SPEAKERS,DICTIONARY)
        val producer = configProducer(HOSTS_PORTS)
        var iterator = 0;
        while (iterator < 10){
            sendRecord(producer, TOPIC,messageGenerator,3)
            Thread.sleep(200)
            iterator+= 1
        }
        
        producer.close()
    }
    
    def configProducer(hostPort: String): KafkaProducer[String, String] = {
        val props = new Properties()
        props.put("bootstrap.servers", hostPort)
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
        new KafkaProducer[String, String](props)
    }
    
    def sendRecord(producer: KafkaProducer[String, String], topic: String, messageGenerator: MessageGenerator, count: Int ): Unit = {
        var iterator = 0;
        while (iterator < count){
            var message = messageGenerator.getMessage()
            producer.send(new ProducerRecord[String, String](topic, "key", messageFormatter(message)))
            iterator+= 1
        }
    }
    
    def messageFormatter(message: Message): String ={
        "{"+
            "\t\n\"speaker\": \"" + message.speaker + "\"," +
            "\t\n\"time\": \"" + message.time + "\"," +
            "\t\n\"word\": \"" + message.word + "\"" +
         "\n}"
    }


}
