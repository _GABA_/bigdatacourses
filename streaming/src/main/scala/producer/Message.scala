package producer

case class Message(speaker: String, time: String, word: String){
    override def toString: String = s"speaker: ${speaker}, time: ${time}, word: ${word}"
    
}
