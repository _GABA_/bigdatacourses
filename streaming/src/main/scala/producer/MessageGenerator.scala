package producer

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import scala.collection.mutable
import scala.util.Random

class MessageGenerator (var speakers: Array[String], var dictionary: Array[String]) {
    
    val TIME_FORMAT = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm:ss.S")
    val SPEAKER_DELAY_TIME = 300
    
    val random = new Random()
    val currentTime = LocalDateTime.now
    val speakerSayTime: mutable.HashMap[String,LocalDateTime] = new mutable.HashMap[String,LocalDateTime]()
    
    def getMessage(): Message = {
        val speaker = speakers(random.nextInt(speakers.length))
        val word = dictionary(random.nextInt(dictionary.length))
        val time = speakerSayTime.getOrElse(speaker,currentTime)
        speakerSayTime.put(speaker, time.plusSeconds(random.nextInt(SPEAKER_DELAY_TIME)))
        Message(speaker,time.format(TIME_FORMAT),word)
    }
    
}
