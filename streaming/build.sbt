import Keys._
import sbtassembly.AssemblyPlugin.autoImport._
lazy val streaming = (project in file(".")).
    settings(
        name := "streaming",
        version := "1.0",
        scalaVersion := "2.11.8",
        mainClass in Compile := Some("producer.Producer")
    ).
enablePlugins(AssemblyPlugin)


name := "streaming"

val kafkaVersion = "2.4.1"

version := "0.1"
scalaVersion := "2.11.8"

libraryDependencies += "org.apache.kafka" % "kafka-clients" % "2.4.1"
libraryDependencies += "org.apache.spark" %% "spark-core" % "2.4.4"
libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.4"

assemblyJarName in assembly := "producer.jar"

assemblyMergeStrategy in assembly := {
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case x => MergeStrategy.first
}

resolvers in Global ++= Seq(
    "Sbt plugins"                   at "https://dl.bintray.com/sbt/sbt-plugin-releases",
    "Maven Central Server"          at "http://repo1.maven.org/maven2",
    "TypeSafe Repository Releases"  at "http://repo.typesafe.com/typesafe/releases/",
    "TypeSafe Repository Snapshots" at "http://repo.typesafe.com/typesafe/snapshots/"
)