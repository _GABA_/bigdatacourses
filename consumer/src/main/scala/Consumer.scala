import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import scala.util.matching.Regex


object Consumer {
    
    val CENSOR_SUBSTITUTE = "***"
    val messagePattern: Regex = """(?s)\{.*"speaker": *"(\w+)", *.*"time": *"(.+)", *.*"word": *"(.+)".*\}""".r
    
    def main(args: Array[String]): Unit = {
        

        if (args.length !=4 ){
            throw new IllegalArgumentException;
        }

        val spark = SparkSession
            .builder()
            .appName("Streaming")
            .master("local")
            .getOrCreate()

        spark.sparkContext.setLogLevel("ERROR")

        val HOSTS_PORTS = args(0)
        val TOPIC = args(1)
        val DICTIONARY: Array[String] = spark.sparkContext.textFile(args(2))
            .flatMap(line => line.split("\n")).collect()
        val WINDOW_DURATION = args(3).toInt

        import spark.implicits._
        val inputStream = spark.readStream
            .format("kafka")
            .option("kafka.bootstrap.servers", HOSTS_PORTS)
            .option("subscribe", TOPIC)
            .load()

        val value = inputStream.selectExpr("CAST(value AS STRING)")
            .as[String]
        
        val message = value
            .flatMap( v => parseMessage(v))
            .map( m => censorMessage(m,DICTIONARY))
            .as[Message]
        

        
        val speakerWordFromWindow = message
            .selectExpr("*", "cast(time as timestamp) as event_time")
            .withWatermark("event_time", "10 minutes")
            .select($"event_time",$"speaker",$"word")
            .groupBy(window(
                    col("event_time")
                    ,WINDOW_DURATION + " minutes"
                    ,WINDOW_DURATION + " minutes") as "window"
                ,$"speaker")
            .agg(collect_list( $"word").as("words")
                ,count("word").as("word_count"))
            .select($"window",$"words",$"speaker",$"word_count")
//            .orderBy($"window".desc)
            .writeStream
            .option("truncate", "false")
            .format("console")
            //.option("truncate", "false")
            .outputMode("update")
            .start()




        val speakerCensors = message
            .filter(m => m.word.equals(CENSOR_SUBSTITUTE))
            .selectExpr("*", "cast(time as timestamp) as event_time")
            //.withWatermark("event_time", "10 minutes")
            .select($"event_time",$"speaker",$"word")
            .groupBy($"speaker")
            .agg(count("word").as("bad_word_count"))
            .select($"speaker",$"bad_word_count")
            .writeStream
            .outputMode("update")
            .format("console")
            .option("truncate", false)
            .start()


        speakerWordFromWindow.awaitTermination()
        speakerCensors.awaitTermination()

        spark.stop()
    }
    
    def parseMessage(text: String):Iterator[Message] = {
        messagePattern.findAllIn(text)
            .matchData
            .map( m =>  Message( m.group(1), m.group(2), m.group(3)) )
    }

    def censorMessage(message: Message,dictionary:Array[String]):Message = {
        if (dictionary.contains(message.word)){
            return Message(message.speaker,message.time,CENSOR_SUBSTITUTE)
        }
        message
    }
    
}

case class Message(speaker: String, time: String, word: String){
    override def toString: String = s"speaker: ${speaker}, time: ${time}, word: ${word}"
    
}
