import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

object DzRDD {
    
    val DELIMITER = "|"
    val PLAYER_COUNT = 10

    def main(args: Array[String]) = {
        val IN_PATH = args(0)
        val OUT_PATH = args(1)
        val spark = SparkSession
            .builder
            .appName("ScalaWordCount")
            .config("spark.master", "local")
            .config("spark.hadoop.validateOutputSpecs", "false")
            .getOrCreate()
        
        val rdd = rddResult(spark, IN_PATH)
        val header: RDD[String] = spark.sparkContext.parallelize(Array("name|awards|goals|teams"))
        val top: RDD[String] = spark.sparkContext.parallelize(rdd.take(PLAYER_COUNT))
        val result = header.union(top)

        result.coalesce(1).saveAsTextFile(OUT_PATH+"txt")
        result.coalesce(1).saveAsObjectFile(OUT_PATH+"obj")
    }
    
    def rddPlayerWithAllTeam(spark: SparkSession, path: String): RDD[(String,String)] = {
        val GOALIES = path + "Scoring.csv"
        spark.sparkContext.textFile(GOALIES).flatMap(line => line.split("\n"))
            .map(line => {
                val elements = line.split(",")
                (elements(0), elements(3))    // id  |  teamId
            })
            .distinct()
    }
    
    def rddPlayerGoals(spark: SparkSession, path: String): RDD[(String,Int)] = {
        val GOALIES = path + "Scoring.csv"
        spark.sparkContext.textFile(GOALIES).flatMap(line => line.split("\n"))
            .map(line => {
                val elements = line.split(",")
                if (elements.length >= 7){
                    (elements(0), goalParser(elements(7)))    // id  |  goals
                }
                else {
                    (elements(0), 0)
                }
            })
            .reduceByKey( (e1, e2) => {
                e1 + e2
            })
    }
    
    def rddPlayerName(spark: SparkSession, path: String): RDD[(String,String)] = {
        val MASTER = path + "Master.csv"
        spark.sparkContext.textFile(MASTER).flatMap(line => line.split("\n"))
            .map(line => {
                val elements = line.split(",")
                (elements(0), elements(3) + " " + elements(4))     //  id  |  firstName   lastName
            })
            .distinct()
            .filter(e => e._1 != "")
    }
    
    def rddTeam(spark: SparkSession, path: String): RDD[(String,String)] = {
        val TEAMS = path + "Teams.csv"
        spark.sparkContext.textFile(TEAMS).flatMap(line => line.split("\n"))
            .map(line => {
                val elements = line.split(",")
                (elements(2), elements(18))        //  teamId  |  teamName
            })
            .distinct()
    }
    
    def rddPlayerTeamsName(spark: SparkSession ,path: String): RDD[(String,Iterable[String])] = {
        val playerWithAllTeam = rddPlayerWithAllTeam(spark,path)
        val team = rddTeam(spark,path)
        
        playerWithAllTeam.map(e => (e._2,e._1))
            .join(team)
            .map(e => (e._2._1, e._2._2))
            .groupByKey()
    }
    
    def rddPlayerAwards(spark: SparkSession, path: String): RDD[(String,Int)] = {
        val AWARDS = path + "AwardsPlayers.csv"
        spark.sparkContext.textFile(AWARDS).flatMap(line => line.split("\n"))
            .map(line => {
                val elements = line.split(",")
                (elements(0),elements(1))
            })
            .map(e => (e._1, 1))
            .reduceByKey( (e1, e2) => {
                e1 + e2
            })
    }
    
    
    def rddResult(spark: SparkSession, path: String): RDD[String] = {
        val playerGoals = rddPlayerGoals(spark,path)
        val playerName = rddPlayerName(spark,path)
        val playerTeamsName = rddPlayerTeamsName(spark,path)
        val playerAwards = rddPlayerAwards(spark,path)
        var counter: Int = 0
        playerName
            .join(playerGoals)
            .join(playerTeamsName)
            .leftOuterJoin(playerAwards)
            .values
            .sortBy(e => e._1._1._2,false)
            .map(e => {
                val teams = e._1._2.reduce((t1,t2) => t1 + ", " + t2 )
                counter += 1
                e._1._1._1 + DELIMITER + e._2.getOrElse(0) + DELIMITER + e._1._1._2 + DELIMITER + teams
            })
    }
    
    def goalParser(s: String): Int = {
        try{
            s.toInt
        } catch {
            case e: Exception => 0
        }
    }
    
}
