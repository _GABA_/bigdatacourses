import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}

object DzSQL {
    
    val SCHEMA_SCORING = StructType(Array(
        StructField("id", StringType, true),
        StructField("year", IntegerType, true),
        StructField("stint", IntegerType, true),
        StructField("tmID", StringType, true),
        StructField("lgID", StringType, true),
        StructField("pos", StringType, true),
        StructField("GP", IntegerType, true),
        StructField("G", IntegerType, true),
        StructField("A", IntegerType, true),
        StructField("Pts", IntegerType, true),
        StructField("PIM", IntegerType, true),
        StructField("pm", IntegerType, true),
        StructField("PPG", StringType, true),
        StructField("PPA", StringType, true),
        StructField("SHG", StringType, true),
        StructField("SHA", StringType, true),
        StructField("GWG", StringType, true),
        StructField("GTG", StringType, true),
        StructField("SOG", StringType, true),
        StructField("PostGP", StringType, true),
        StructField("PostG", StringType, true),
        StructField("PostA", StringType, true),
        StructField("PostPts", StringType, true),
        StructField("PostPIM", StringType, true),
        StructField("Postpm", StringType, true),
        StructField("PostPPG", StringType, true),
        StructField("PostPPA", StringType, true),
        StructField("PostSHG", StringType, true),
        StructField("PostSHA", StringType, true),
        StructField("PostGWG", StringType, true),
        StructField("PostSOG", IntegerType, true)
    ))
    val SCHEMA_MASTER = StructType(Array(
        StructField("id", StringType, true),
        StructField("coachID", StringType, true),
        StructField("hofID", StringType, true),
        StructField("firstName", StringType, true),
        StructField("lastName", StringType, true),
        StructField("nameNote", StringType, true),
        StructField("nameGiven", StringType, true),
        StructField("nameNick", StringType, true),
        StructField("height", StringType, true),
        StructField("weight", StringType, true),
        StructField("shootCatch", StringType, true),
        StructField("legendsID", StringType, true),
        StructField("ihdbID", StringType, true),
        StructField("hrefID", StringType, true),
        StructField("firstNHL", StringType, true),
        StructField("lastNHL", StringType, true),
        StructField("firstWHA", StringType, true),
        StructField("lastWHA", StringType, true),
        StructField("pos", StringType, true),
        StructField("birthYear", StringType, true),
        StructField("birthMon", StringType, true),
        StructField("birthDay", StringType, true),
        StructField("birthCountry", StringType, true),
        StructField("birthState", StringType, true),
        StructField("birthCity", StringType, true),
        StructField("deathYear", StringType, true),
        StructField("deathMon", StringType, true),
        StructField("deathDay", StringType, true),
        StructField("deathCountry", StringType, true),
        StructField("deathState", StringType, true),
        StructField("deathCity", StringType, true)
    ))
    val SCHEMA_TEAMS = StructType(Array(
        StructField("year", StringType, true),
        StructField("lgID", StringType, true),
        StructField("tmID", StringType, true),
        StructField("franchID", StringType, true),
        StructField("confID", StringType, true),
        StructField("divID", StringType, true),
        StructField("rank", StringType, true),
        StructField("playoff", StringType, true),
        StructField("G", StringType, true),
        StructField("W", StringType, true),
        StructField("L", StringType, true),
        StructField("T", StringType, true),
        StructField("OTL", StringType, true),
        StructField("Pts", StringType, true),
        StructField("SoW", StringType, true),
        StructField("SoL", StringType, true),
        StructField("GF", StringType, true),
        StructField("GA", StringType, true),
        StructField("name", StringType, true),
        StructField("PIM", StringType, true),
        StructField("BenchMinor", StringType, true),
        StructField("PPG", StringType, true),
        StructField("PPC", StringType, true),
        StructField("SHA", StringType, true),
        StructField("PKG", StringType, true),
        StructField("PKC", StringType, true),
        StructField("SHF", StringType, true)
    ))
    val SCHEMA_AWARDS = StructType(Array(
        StructField("id", StringType, true),
        StructField("award", StringType, true),
        StructField("year", StringType, true),
        StructField("lgID", StringType, true),
        StructField("note", StringType, true),
        StructField("pos", StringType, true)
    ))
    
    def main(args: Array[String]): Unit = {
        
        val spark = SparkSession
            .builder
            .appName("ScalaWordCount")
            .config("spark.master", "local")
            .config("spark.hadoop.validateOutputSpecs", "false")
            .config("header","true")
            .getOrCreate()
        
        sql(spark,args(0),args(1))
    }
    
    def sql(spark: SparkSession, inPath: String, outPath: String): Unit = {
        
        val SCORING = inPath + "Scoring.csv"
        val MASTER = inPath + "Master.csv"
        val TEAMS = inPath + "Teams.csv"
        val AWARDS = inPath + "AwardsPlayers.csv"
        
        createTmpTable(spark, SCHEMA_SCORING,SCORING,"scoring")
        createTmpTable(spark, SCHEMA_MASTER,MASTER,"master")
        createTmpTable(spark, SCHEMA_TEAMS,TEAMS,"teams")
        createTmpTable(spark, SCHEMA_AWARDS,AWARDS,"awards")
        
        spark.sql("SELECT DISTINCT " +
            " s.id" +
            ", concat_ws(', ', collect_set(t.name)) as teams " +
            " FROM scoring s " +
            " JOIN teams t ON s.tmID = t.tmID " +
            " GROUP BY s.id"
        ).createOrReplaceTempView("player_teams_name")
    
        spark.sql("SELECT " +
            " id" +
            ", count(award) as awards" +
            " FROM awards  " +
            " GROUP BY id"
        ).createOrReplaceTempView("player_awards")
        
        spark.sql(" SELECT DISTINCT " +
            " m.id, " +
            " CONCAT(m.firstName,\" \", m.lastName) as fullName" +
            ", SUM(G) as goals" +
            " FROM master m " +
            " JOIN scoring s ON m.id = s.id " +
            " WHERE m.id IS NOT NULL " +
            " AND s.G IS NOT NULL " +
            " GROUP BY m.id, fullName "
        ).createOrReplaceTempView("player_goals")
        
        val result = spark.sql("SELECT fullName, awards, goals, teams" +
            " FROM player_teams_name" +
            " JOIN player_goals" +
            " ON player_teams_name.id = player_goals.id " +
            " LEFT JOIN player_awards" +
            " ON player_teams_name.id = player_awards.id " +
            " ORDER BY goals DESC ")
        
        val top = result.limit(10)
    
        top.write
            .mode("overwrite")
            .format("parquet")
            .save(outPath)
    
    }
    
    def createTmpTable(spark: SparkSession, schema: StructType, path: String, name: String): Unit  = {
        spark.read.format("csv").schema(schema).load(path).toDF().createOrReplaceTempView(name)
    }
}
