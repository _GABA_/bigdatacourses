import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.IntegerType

object DzDS {
    
    case class Scoring(id: String, year: String, stint: String, tmID: String,
        lgID: String, pos: String, GP: String, G: Integer, A: String, Pts: String,
        PIM: String, pm: String, PPG: String, PPA: String, SHG: String, SHA: String,
        GWG: String, GTG: String, SOG: String, PostGP: String, PostG: String,
        PostA: String, PostPts: String, PostPIM: String, Postpm: String, PostPPG: String,
        PostPPA: String, PostSHG: String, PostSHA: String, PostGWG: String, PostSOG: String)
        
    case class Master ( id: String, coachID: String, hofID: String, firstName: String,
        lastName: String, nameNote: String, nameGiven: String, nameNick: String,
        height: String, weight: String, shootCatch: String, legendsID: String,
        ihdbID: String, hrefID: String, firstNHL: String, lastNHL: String,
        firstWHA: String, lastWHA: String, pos: String, birthYear: String,
        birthMon: String, birthDay: String, birthCountry: String, birthState: String,
        birthCity: String, deathYear: String, deathMon: String, deathDay: String,
        deathCountry: String, deathState: String, deathCity: String)
    
    case class Teams ( year: String, lgID: String, tmID: String,
        franchID: String, confID: String, divID: String, rank: String,
        playoff: String, G: String, W: String, L: String, T: String,
        OTL: String, Pts: String, SoW: String, SoL: String, GF: String,
        GA: String, name: String, PIM: String, BenchMinor: String, PPG: String,
        PPC: String, SHA: String, PKG: String, PKC: String, SHF: String)
    
    case class Awards ( id: String, award: String,year: String,
        lgID: String, note: String, pos: String)

    
    def main(args: Array[String]): Unit = {
    
        val IN_PATH = args(0);
        val OUT_PATH = args(1);
        val SCORING = IN_PATH + "Scoring.csv"
        val MASTER = IN_PATH + "Master.csv"
        val TEAMS = IN_PATH + "Teams.csv"
        val AWARDS = IN_PATH + "AwardsPlayers.csv"
        
        val spark = SparkSession
            .builder
            .appName("ScalaWordCount")
            .config("spark.master", "local")
            .config("spark.hadoop.validateOutputSpecs", "false")
            .config("header","true")
            .getOrCreate()
    
        import spark.implicits._
        val scoring = spark.read.option("header", "true").csv(SCORING)
            .withColumn("id", col("playerID"))
            .withColumn("pm", col("+/-"))
            .withColumn("Postpm", col("Post+/-"))
            .withColumn("G", col("G").cast(IntegerType))
            .as[Scoring]
        val master = spark.read.option("header", "true").csv(MASTER)
            .withColumn("id", col("playerID")).as[Master]
        val teams = spark.read.option("header", "true").csv(TEAMS).as[Teams]
        val awards = spark.read.option("header", "true").csv(AWARDS)
            .withColumn("id", col("playerID")).as[Awards]
    
        val playerTeams = scoring
            .join(teams, scoring("tmID").equalTo(teams("tmID")))
            .select( scoring("id"), teams("name"))
            .distinct()
        
        val playerGoals = scoring
            .groupBy('id)
            .agg(sum('G).as("goals"))
    
        val playerTeamsNames = playerTeams
            .groupBy('id)
            .agg(collect_set('name).as("teams"))
    
        val palyerAwards = awards
            .groupBy('id)
            .agg(count('award).as("awards"))
            .select('id,'awards)
            
        val playerTeamsGoals = playerGoals
            .join(playerTeamsNames,
                playerGoals("id").equalTo(playerTeamsNames("id"))
            )
            .select( playerGoals("id"), 'goals, 'teams )
        
        val playerTGA = playerTeamsGoals
            .join(palyerAwards,
                playerGoals("id") === palyerAwards("id"),"leftouter"
            ).select(playerGoals("id"), 'goals, 'teams,'awards)
        
        val result = playerTGA
            .join( master,
                playerTGA("id").equalTo(master("id"))
            )
            .select(
                concat(master("firstName"),lit(" "),
                    master("lastName")).as("name"),
                'awards,
                playerTGA("goals"),
                concat_ws(", ", playerTGA("teams")).as("teams")
            )
            .orderBy('goals desc)
        
        val top = result.limit(10)
    
        top.write
            .mode("overwrite")
            .option("header", "true")
            .format("com.databricks.spark.csv")
            .save(OUT_PATH)

    }
}

